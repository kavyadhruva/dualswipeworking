package com.example.myapplication;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by omarfaroque on 4/17/16.
 */
public class ViewHolderListItem extends RecyclerView.ViewHolder {
    @Bind(R.id.text1)
    TextView tvName;
    @Bind(R.id.text2)
    TextView tvCell;


    public void setTvName(String name) {
        tvName.setText(name);
    }

    public void setTvCell(String cell) {
        tvCell.setText(cell);
    }


    public ViewHolderListItem(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}
