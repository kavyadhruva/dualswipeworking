package com.example.myapplication;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by omarfaroque on 4/18/16.
 */
public class ViewHolderHeader extends RecyclerView.ViewHolder {

    @Bind(R.id.header)
    TextView tvHeader;

    public void setTvHeader(String header) {
        tvHeader.setText(header);
    }

    public ViewHolderHeader(View view) {
        super(view);
        ButterKnife.bind(this,view);
    }
}
